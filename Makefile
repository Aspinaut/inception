all:
	@if [ ! -d "/home/vmasse/data" ]; then\
		sudo mkdir -p /home/vmasse/data/wp;\
		sudo mkdir -p /home/vmasse/data/db;\
		sudo chmod 777 /var/run/docker.sock;\
		echo "127.0.0.1		vmasse.42.fr" >> /etc/hosts;\
		docker-compose -f srcs/docker-compose.yml up -d --build;\
	else\
		echo "Error: /home/vmasse/data already exists.\nTry \"make re\"";\
	fi

clean:
	@echo "Cleaning everything except volumes...\n"
	@sudo docker system prune -f 

fclean:
	@if [ -d "/home/vmasse/data" ]; then\
		echo "Cleaning everything...\n";\
		docker-compose -f srcs/docker-compose.yml down --volumes --rmi all;\
		sudo docker image prune -a -f;\
		sudo sed -i '/127.0.0.1		vmasse.42.fr/d' /etc/hosts;\
		sudo rm -rf /home/vmasse/data;\
	else\
		echo "Error: Nothing to clear.";\
	fi

list:
	@echo "------------------ VOLUMES ------------------------\n"
	@sudo docker volume ls
	@echo "------------------ CONTAINERS ------------------------\n"
	@sudo docker ps -a
	@echo "------------------ IMAGES ------------------------\n"
	@sudo docker images
	@echo "------------------ NETWORKS ------------------------\n"
	@sudo docker network ls

re:	fclean all

.PHONY:	all clean fclean re list
