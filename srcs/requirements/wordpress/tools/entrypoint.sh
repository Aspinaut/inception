echo "Checking if wp-config.php exists"
if [ ! -f "/var/www/wp-config.php" ]; then
	echo "wp-config.php not found, configuring wp..."
	wp config create --dbname="$DB_NAME" --dbuser="$DB_USER" --dbpass="$DB_USER_PWD" --dbhost="$DB_HOST" --path="/var/www/" --dbprefix="wp_"
	wp core install --url="$DOMAIN_NAME" --title="vmasse's inception" --admin_user="$WP_ADMIN" --admin_password="$WP_ADMIN_PWD" --admin_email="$WP_ADMIN_EMAIL" --path="/var/www/"
	wp user create $WP_USER $WP_USER_EMAIL --role="author" --user_pass="$WP_USER_PWD" --path="/var/www"
	wp theme install astra --activate --path="/var/www" --allow-root 
else
	echo "wp-config.php found, skipping configuration"
fi

php-fpm7 -F
