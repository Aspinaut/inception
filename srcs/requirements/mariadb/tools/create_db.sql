USE mysql;
FLUSH PRIVILEGES;
DELETE FROM	mysql.user WHERE User='';
DROP DATABASE test;
DELETE FROM mysql.db WHERE Db='test';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');

CREATE DATABASE wordpress;
CREATE USER 'user42'@'%' IDENTIFIED BY 'superp4ss'; 
GRANT ALL PRIVILEGES ON wordpress.* TO 'user42'@'%';
FLUSH PRIVILEGES;
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('superp4ss');
