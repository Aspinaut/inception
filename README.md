# Inception

## About

School 42 project using Docker. \
It uses 3 different services :
* MariaDB
* Wordpress
* Nginx

It is meant to launch a basic local website using nginx server \
It's working on UNIX environments.

***

## How to run it

You'll need to install the docker-compose command \
Modify HOME_PATH in Makefile line 1 with your username

```
git clone https://gitlab.com/Ralphiki/Inception.git
cd Inception
sudo make
```

Then see the wordpress website at 127.0.0.1 
